package javafxapplication1;

public class Teste {

    private Pessoa p1;
    private Conta c1;
    
    public Teste() {
        
    }
    public Pessoa criarPessoa(){
        this.p1 = new Pessoa("ana", "rua x", "xyz","y", "z", 54454, "ana@xyz.com", "12345678964", "0339948856");
        return p1;
    }
    public Conta criarConta(int nConta){
        this.c1 = new Conta (p1, 1,438, 0,nConta);
        return c1;
    }
}
