package javafxapplication1;

public final class Pessoa {
  private String nome;
  private String endereco;
  private String cidade;
  private String bairro;
  private String estado;
  private int fone;
  private String email;
  private String CPF;
  private String RG;

  public Pessoa(String nome, String end, String cid, String bairro,String est, int fone, String email, String cpf, String rg){
    setNome(nome);
    setEndereco(end);
    this.cidade = cid;
    this.bairro = bairro;
    this.estado = est;
    this.fone = fone;
    this.email = email;
    this.CPF = cpf;
    this.RG =rg;
  }
  public void setNome (String nome){
    this.nome = nome;
  }
  public String getNome (){
    return nome;
  }
  public void setCidade (String cidade){
    this.cidade = cidade;
  }
  public String getCidade (){
    return cidade;
  }
  public void setEndereco (String endereco){
    this.endereco = endereco;
  }
  public String getEndereco (){
    return endereco;
  }
  public void setBairro (String bairro){
    this.bairro = bairro;
  }
  public String getBairro (){
    return bairro;
  }
  public void setEstado (String estado){
    this.estado = estado;
  }
  public String getEstado (){
    return estado;
  }
  public void setFone (int fone){
    this.fone = fone;
  }
  public int getFone (){
    return fone;
  }
  public void setEmail (String email){
    this.email = email;
  }
  public String getEmail (){
    return email;
  }
  public void setCPF (String CPF){
    this.CPF = CPF;
  }
  public String getCPF (){
    return CPF;
  }
  public void setRG (String RG){
    this.RG = RG;
  }
  public String getRG (){
      return RG;
  }
  
  
}
