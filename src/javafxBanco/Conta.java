package javafxBanco;

public class Conta {
  private final Pessoa titular;
  private final int numConta;
  private final int digito;
  private double saldo;
  private final int codConta;
  
  public Conta(Pessoa titular, int dig, int con, double saldo, int codConta){
    this.titular = titular;
    this.saldo = saldo;
    this.digito = dig;
    this.numConta = con;
    this.codConta = codConta;
  }
  
  public Pessoa getTitular(){
    return titular;
  }
  public double getSaldo(){
    return saldo;
  }
  public int getDigito(){
    return digito;
  }
  public int getConta(){
    return numConta;
  }
  public int getCodConta(){
    return codConta;
  }
  
  boolean debita (double val){
    if (this.saldo>= val){
        this.saldo -= val;
        return true;
    }
    else{
        return false;
    }
  }
  boolean credita (double val){
    this.saldo += val;
    return true;
  }

  
}
