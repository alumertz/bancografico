package javafxBanco;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class FXMLBancoController implements Initializable {
    
    private final Conta contas[] = new Conta[3];
    private Pessoa p1;
    private int nCliente=0;
    private int nConta=0;
    private int quantContas=0;
    private int contaManipulada;
    private Teste t1 = new Teste();
    
    @FXML
    private TextField nome, bairro,cidade,endereco, estado,telefone,email,cpf,rg,saldo,conta,digito,credito,debito, outraConta;

    @FXML
    private Label confirma,mostraSaldo,mostraConta;

    @FXML
    private void botaoCreditar(ActionEvent event) {
        double creditar = Double.parseDouble(credito.getText());
        if (contas[contaManipulada].credita(creditar)){
            confirma.setText("Valor creditado com sucesso!");
        }
        mostraSaldo.setText (Double.toString(contas[contaManipulada].getSaldo()));
        
    }
    @FXML
    private void botaoDebitar(ActionEvent event) {
        double debitar = Double.parseDouble(debito.getText()); 
        if (contas[contaManipulada].debita(debitar)){
            confirma.setText("Valor debitado com sucesso!");
        }
        else{
             confirma.setText("Você não tem saldo suficiente, o valor não pode ser debitado com sucesso.");
        }
        mostraSaldo.setText (Double.toString(contas[contaManipulada].getSaldo()));
        
    }
    @FXML
    private void botaoLimparDados(ActionEvent event) {
        p1 = null;
        limpar (); 
        nConta=0;
        contaManipulada=0;
        Conta contas[] = new Conta[3];
    }
    private void limpar(){
        nome.setText("");
        endereco.setText("");
        bairro.setText("");
        cidade.setText("");
        estado.setText("");
        email.setText("");
        cpf.setText("");
        rg.setText("");
        telefone.setText("");
        saldo.setText("");
        conta.setText("");
        digito.setText("");
        confirma.setText("");
        mostraSaldo.setText("");
        mostraConta.setText("");
        outraConta.setText("");
    }
    @FXML
    private void botaoCriarConta(ActionEvent event) throws InterruptedException{
        int numConta, nDigito;
        double nSaldo;
        
        if (nConta<3){
            nSaldo = Double.parseDouble(saldo.getText());
            numConta = Integer.parseInt(conta.getText());
            nDigito = Integer.parseInt(digito.getText());
            contas[nConta]= new Conta (p1, nDigito,numConta,nSaldo,nConta);
            contaManipulada = nConta;
            mostraSaldo.setText(Double.toString(contas[contaManipulada].getSaldo()));
            mostraConta.setText(Integer.toString(contas[contaManipulada].getConta())); 
            confirma.setText("Conta Criada");
            nConta++;
            quantContas++;
        }
        else{
            confirma.setText("Limite de contas atingido, conta não criada.");
        }
        limpar();
        mostraSaldo.setText(Double.toString(contas[contaManipulada].getSaldo()));
        mostraConta.setText(Integer.toString(contas[contaManipulada].getConta()));
        
    }
    @FXML
    private void botaoTrocar(ActionEvent event){
        int conta = Integer.parseInt(outraConta.getText());
        limpar();
        for (int x=0; x<quantContas; x++){
            if (conta== contas[x].getConta()){
                contaManipulada = contas[x].getCodConta();
            }
        }
        limpar();
        mostraSaldo.setText(Double.toString(contas[contaManipulada].getSaldo()));
        mostraConta.setText(Integer.toString(contas[contaManipulada].getConta()));
        confirma.setText ("Conta alterada");
    }
    
    @FXML
    private void botaoCadastrarTudo(ActionEvent event) {
        String name, end,cid, bair,est,eMail, CPF, RG ;
        int fone,con, dig;
        double sald;
        if (nCliente==0){
            p1 = t1.criarPessoa();
            
            contas [nConta] = t1.criarConta(nConta);
        }
        else{
            name = nome.getText();
            end = endereco.getText();
            bair = bairro.getText();
            cid = cidade.getText();
            est = estado.getText();
            eMail = email.getText();
            CPF = cpf.getText();
            RG = rg.getText();
            fone = Integer.parseInt(telefone.getText());
            sald = Double.parseDouble(saldo.getText());
            con = Integer.parseInt(conta.getText());
            dig = Integer.parseInt(digito.getText());
            
            p1 = new Pessoa(name, end, cid,bair, est, fone,eMail, CPF, RG);
            System.out.println("nConta"+ nConta);
            contas[nConta] = new Conta(p1, dig,con, sald, nConta);
        }
        contaManipulada = nConta;
        quantContas++;
        nConta++;
        nCliente++;
        mostraSaldo.setText(Double.toString(contas[contaManipulada].getSaldo()));
        mostraConta.setText(Integer.toString(contas[contaManipulada].getConta())); 
        confirma.setText("Cliente cadastrado!");
        
    }
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   
    
}
